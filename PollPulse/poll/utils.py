# poll/utils.py
import os
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
from django.template.loader import render_to_string
import logging
logger = logging.getLogger(__name__)
def create_user(username, email, password):
    user = User.objects.create_user(username=username, email=email, password=password)
    return user
def load_cuss_words():
    file_path = os.path.join(os.path.dirname(__file__), 'data', 'cuss_words.txt')
    with open(file_path, 'r') as file:
        cuss_words = file.read().splitlines()  # Read all lines into a list
    return cuss_words
def validate_poll_question(question):
    """Check if the poll question contains any cuss words."""
    cuss_words = load_cuss_words()
    question_words = question.lower().split()
    
    return not any(word.strip('.,!?;:') in cuss_words for word in question_words)

def send_verification_email(user):
    token = user.profile.verification_token
    verification_url = reverse('poll:activate_account', kwargs={'uidb64': user.profile.uidb64, 'token': token})
    verification_link = f"{settings.DOMAIN_NAME}{verification_url}"
    logger.info(f"Generated token for user {user.username}: {token}")

    subject = 'Verify your email address'
    context = {
        'user': user,
        'domain': settings.DOMAIN_NAME,
        'uid': user.profile.uidb64,
        'token': token
    }
    message = render_to_string('poll/activate_account_email.html', context)

    email = EmailMessage(
        subject,
        message,
        settings.DEFAULT_FROM_EMAIL,
        [user.email]
    )
    email.content_subtype = "html"  # For HTML email
    email.send()
