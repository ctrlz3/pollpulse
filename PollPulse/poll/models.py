from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
import hashlib
import os
from .utils import validate_poll_question

class Poll(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', default=timezone.now)
    end_date = models.DateTimeField('date ended', null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='polls')
    is_multiple_choice = models.BooleanField(default=False)
    choices_hash = models.CharField(max_length=64, editable=False, blank=True)

    def generate_choices_hash(self):
        if not self.pk:
            return ''
        # Get all the choices related to this poll and sort them
        choices = self.choices.all().values_list('choice_text', flat=True)
        # Convert choices list to a sorted string
        choices_string = ''.join(sorted(choices))
        # Generate the hash
        return hashlib.sha256(choices_string.encode('utf-8')).hexdigest()

    def save(self, *args, **kwargs):
        # Ensure the choices hash is generated before saving the poll
        super().save(*args, **kwargs)
        self.choices_hash = self.generate_choices_hash()
        super().save(update_fields=['choices_hash'])

    def __str__(self):
        return self.question

    def is_active(self):
        # Check if poll is still active
        if self.end_date:
            return timezone.now() < self.end_date
        return True

    def clean(self):
        # Validate poll question against inappropriate language
        if not validate_poll_question(self.question):
            raise ValidationError("Poll question contains inappropriate language.")

    @staticmethod
    def load_cuss_words():
        """
        Loads the list of cuss words from the cuss_words.txt file and returns it as a list.
        """
        file_path = os.path.join(os.path.dirname(__file__), 'data', 'cuss_words.txt')
        with open(file_path, 'r') as file:
            cuss_words = file.read().splitlines()  # Read all lines into a list
        return cuss_words


class Choice(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name='choices')
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class Report(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    reason = models.CharField(max_length=100)
    details = models.TextField(blank=True)

    def __str__(self):
        return f"Report for Poll {self.poll.id} by User {self.user.id}"


class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'choice')


class Comment(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name='comments')
    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment_text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Comment by {self.user.username} on Poll {self.poll.id}"


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_verified = models.BooleanField(default=False)
    verification_token = models.CharField(max_length=64, blank=True)
    uidb64 = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return f"{self.user.username} Profile"

    def save(self, *args, **kwargs):
        # Generate a verification token and uidb64 if not already set
        if not self.verification_token:
            self.verification_token = get_random_string(64)
        if not self.uidb64:
            self.uidb64 = urlsafe_base64_encode(force_bytes(self.user.pk))
        super().save(*args, **kwargs)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
