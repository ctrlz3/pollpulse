from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django import forms
from django.forms.widgets import PasswordInput, TextInput
from django.forms import modelformset_factory
from .models import Poll, Choice
from .models import Report, Comment
import hashlib

# Create/Register a user (Model Form)
class CreateUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('An account with this email already exists.')
        return email

# Authenticate a user (Model Form)
class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput())
    password = forms.CharField(widget=PasswordInput())

class PollForm(forms.ModelForm):
    end_date = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}),
        required=False
    )

    class Meta:
        model = Poll
        fields = ['question', 'end_date', 'is_multiple_choice']

    def clean(self):
        cleaned_data = super().clean()
        question = cleaned_data.get('question')
        
        if Poll.objects.filter(question = question).exists():
            raise forms.ValidationError("A poll with this question already exists")

        return cleaned_data



class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['choice_text']
ChoiceFormSet = modelformset_factory(Choice, form=ChoiceForm, extra=2)  # To manage multiple choice creation

class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['reason', 'details']
        widgets = {
            'details': forms.Textarea(attrs={'rows': 4, 'cols': 40}),
        }
class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment_text']
        widgets = {
            'comment_text': forms.Textarea(attrs={'placeholder': 'Write your comment here...', 'rows': 4}),
        }

