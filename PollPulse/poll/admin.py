from django.contrib import admin
from .models import Poll,Choice,Report,Profile

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class PollAdmin( admin.ModelAdmin):
    fieldsets =[
        (None , {'fields' : ['question']}),
        ('Date Information', { 'fields' : ['pub_date', 'end_date'] , 'classes' : ['collapse']}),

    ]
    inlines = [ChoiceInline]
    list_display = ('question', 'pub_date', 'end_date', 'is_active')
    list_filter = ['pub_date']
    search_fields = ['question']
    actions = ['delete_reported_polls']

    def delete_reported_polls(self,request,queryset):
        deleted_count = 0 #To keep track of how many polls have been deleted during execution of task

   
        for poll in queryset:
            if Report.objects.filter(poll=poll).exists():
                poll.delete()
                deleted_count += 1
        if deleted_count > 0:
            self.message_user(request, f"Successfully deleted {deleted_count} reported poll(s).", messages.SUCCESS)
        else:
            self.message_user(request, "No reported polls found in the selected items.", messages.WARNING)

    delete_reported_polls.short_description = "Delete selected reported polls"


                

 
admin.site.register(Poll, PollAdmin)
admin.site.register(Report)
admin.site.register(Profile)

