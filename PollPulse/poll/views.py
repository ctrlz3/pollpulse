from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import force_str, force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.urls import reverse
from .models import Poll, Choice, Vote, Comment
from .forms import CreateUserForm, LoginForm, PollForm, ChoiceForm, ReportForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout as auth_logout
from django.forms import modelformset_factory
from django.utils import timezone
from django.contrib import messages
from .utils import create_user, validate_poll_question, send_verification_email
from django.db import transaction
from .forms import CommentForm



from django.conf import settings
import logging

def home(request):
    return render(request, 'poll/home.html')

def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False  # Deactivate account until email verification
            user.save()

            send_verification_email(user)
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account was created for {username}. Please check your email to verify your account.')
            return redirect('poll:login')
        else:
            messages.error(request, 'Please correct the errors below.')


            print(form.errors)
            for field, errors in form.errors.items():
                for error in errors:
                    print(f"Error in {field}: {error}")

    context = {'registerform': form}
    return render(request, 'poll/register.html', context=context)

def user_login(request):
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if hasattr(user, 'profile') and user.profile.email_verified:
                    login(request, user)
                    return redirect('poll:dashboard')
                else:
                    messages.error(request, 'Please verify your email address before logging in.')
            else:
                messages.error(request, 'Invalid username or password.')
    context = {'loginform': form}
    return render(request, 'poll/login.html', context=context)

@login_required
def dashboard(request):
    return render(request, 'poll/dashboard.html')

@login_required
def create_poll(request):
    ChoiceFormSet = modelformset_factory(Choice, form=ChoiceForm, extra=2)

    if request.method == 'POST':
        poll_form = PollForm(request.POST)
        formset = ChoiceFormSet(request.POST, queryset=Choice.objects.none())
        
        if poll_form.is_valid() and formset.is_valid():
            poll = poll_form.save(commit=False)
            poll.created_by = request.user
            poll.pub_date = timezone.now()
            poll.save()  # Save poll first
            
            # Ensure the poll has a primary key
            if not poll.id:
                messages.error(request, 'The poll could not be saved.')
                return render(request, 'poll/create_poll.html', {'poll_form': poll_form, 'formset': formset})
            
            # Save choices
            for form in formset.cleaned_data:
                if form:
                    choice_text = form['choice_text']
                    try:
                        Choice.objects.create(poll=poll, choice_text=choice_text, votes=0)
                    except Exception as e:
                        print(f"Error saving choice '{choice_text}': {e}")
                        messages.error(request, 'There was an error saving the poll choices.')
                        return render(request, 'poll/create_poll.html', {'poll_form': poll_form, 'formset': formset})

            return redirect('poll:dashboard')
        else:
            print(f"Poll form errors: {poll_form.errors}")
            print(f"Formset errors: {formset.errors}")
            messages.error(request, 'There were errors in the form.')
    else:
        poll_form = PollForm()
        formset = ChoiceFormSet(queryset=Choice.objects.none())

    return render(request, 'poll/create_poll.html', {'poll_form': poll_form, 'formset': formset})


@login_required
def user_logout(request):
    auth_logout(request)
    return redirect('poll:home')

@login_required
def feedback(request):
    user_polls = Poll.objects.filter(created_by=request.user).order_by('-pub_date')
    
    polls_with_choices_and_comments = []
    for poll in user_polls:
        choices = poll.choices.all()
        comments = poll.comments.all()  # Fetch comments related to the poll
        poll_data = {
            'poll': poll,
            'choices': choices,
            'comments': comments
        }
        polls_with_choices_and_comments.append(poll_data)
    
    return render(request, 'poll/feedback.html', {'polls_with_choices': polls_with_choices_and_comments})


def detail(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)

    if request.method == 'POST' and 'comment' in request.POST:
        comment_text = request.POST['comment']
        if request.user.is_authenticated and comment_text:
            Comment.objects.create(poll=poll, user=request.user, comment_text=comment_text)
            return redirect('poll:detail', poll_id=poll.id)

    comments = Comment.objects.filter(poll=poll) if request.user == poll.created_by else Comment.objects.filter(poll=poll, user=request.user)

    return render(request, 'poll/detail.html', {
        'poll': poll,
        'comments': comments
    })

@login_required
def vote(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)
    if not poll.is_active():
        return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "Poll is closed."})

    if poll.is_multiple_choice:
        selected_choices = request.POST.getlist('choices')
        if not selected_choices:
            return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "You didn't select any choice."})

        if Vote.objects.filter(user=request.user, choice__poll=poll).exists():
            return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "You have already voted."})

        for choice_id in selected_choices:
            try:
                selected_choice = poll.choices.get(pk=choice_id)
                selected_choice.votes += 1
                selected_choice.save()
                Vote.objects.create(user=request.user, choice=selected_choice)
            except Choice.DoesNotExist:
                return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "Invalid choice."})
    else:
        try:
            selected_choice = poll.choices.get(pk=request.POST['choice'])
            if Vote.objects.filter(user=request.user, choice=selected_choice).exists():
                return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "You have already voted."})
            selected_choice.votes += 1
            selected_choice.save()
            Vote.objects.create(user=request.user, choice=selected_choice)
        except (KeyError, Choice.DoesNotExist):
            return render(request, 'poll/detail.html', {'poll': poll, 'error_message': "You didn't select a choice."})

    return HttpResponseRedirect(reverse('poll:results', args=(poll.id,)))

def results(request, poll_id):
    poll = get_object_or_404(Poll, pk=poll_id)

    # Calculate total votes
    total_votes = float(sum(choice.votes for choice in poll.choices.all()))
    
    # Calculate choices with their percentage
    choices_with_percentage = [
        {
            'choice_text': choice.choice_text,
            'votes': choice.votes,
            'percentage': (choice.votes / total_votes) * 100 if total_votes > 0 else 0
        }
        for choice in poll.choices.all()
    ]
    
    # Handle comment form submission
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.poll = poll
            comment.user = request.user
            comment.save()
            return redirect('poll:results', poll_id=poll_id)
    else:
        comment_form = CommentForm()

    # Get comments for the poll
    comments = poll.comments.all()
    
    # Check if the current user is the creator of the poll
    is_creator = request.user == poll.created_by

    # Prepare context
    context = {
        'poll': poll,
        'total_votes': total_votes,
        'choices': choices_with_percentage,
        'comment_form': comment_form,
        'comments': comments,
        'is_creator': is_creator,
    }
    
    return render(request, 'poll/results.html', context)
@login_required
def participate_view(request):
    polls = Poll.objects.all().order_by('-pub_date')
    user_votes = Vote.objects.filter(user=request.user).values_list('choice__poll', flat=True)
    for poll in polls:
        poll.has_voted = poll.id in user_votes

    return render(request, 'poll/participate.html', {'polls': polls})

@login_required
def report_poll(request, poll_id):
    poll = get_object_or_404(Poll, id=poll_id)

    if request.method == 'POST':
        form = ReportForm(request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.poll = poll
            report.user = request.user
            report.save()
            messages.success(request, 'Thank you for your feedback. The poll has been reported.')
            return redirect('poll:dashboard')
    else:
        form = ReportForm()

    return render(request, 'poll/report_poll.html', {'form': form, 'poll': poll})

logger = logging.getLogger(__name__)
def activate_account(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        logger.warning(f"Invalid user ID in activation link: {uidb64}")
    
    if user:
        # Log the token being checked and the token generated for the user
        generated_token = default_token_generator.make_token(user)
        logger.info(f"Generated token for user {user.username}: {generated_token}")
        logger.info(f"Checking token: {token} for user {user.username}")
        logger.info(f"UID before encoding: {uid}")
        logger.info(f"UID after encoding: {uidb64}")
        logger.info(f"Token from profile: {user.profile.verification_token}")
        profile_token = user.profile.verification_token
        logger.info(f"Token from profile: {profile_token}")

    if user is not None and token == user.profile.verification_token:
        user.is_active = True
        user.profile.email_verified = True
        user.profile.save()
        user.save()

        login(request, user)
        messages.success(request, 'Thank you for verifying your email! Your account is now activated.')
        logger.info(f'User {user.username} activated successfully.')
        return redirect('poll:dashboard')
    else:
        if hasattr(user, 'profile'):
            user.profile.email_verified = True
            user.profile.save()
        logger.error(f'Invalid or expired activation link for user: {uidb64}')
        messages.error(request, 'Activation link is invalid or has expired.')
        return redirect('poll:login')

@login_required
def delete_comment(request, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    poll = comment.poll

    # Ensure the user is the creator of the poll
    if request.user == poll.created_by:
        comment.delete()

    # Redirect to the previous page (HTTP_REFERER), or fallback to the poll's feedback page
    referer = request.META.get('HTTP_REFERER')  # Get the previous page URL
    if referer:
        return redirect(referer)
    else:
        return redirect('poll:feedback', poll_id=poll.id)  # Fallback if HTTP_REFERER is missing

