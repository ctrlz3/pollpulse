from django.urls import path
from . import views
from .views import activate_account

app_name = 'poll'

urlpatterns = [

    path('', views.home, name="home"),  # Root URL mapped to home view
    path('register/', views.register, name='register'),  # User registration
    path('login/', views.user_login, name='login'),  # User login
    path('logout/', views.user_logout, name="logout"),  # User logout
    path('polls/create/', views.create_poll, name='create_poll'),  # Create a poll
    path('dashboard/', views.dashboard, name='dashboard'),  # User dashboard
    path('participate/', views.participate_view, name='participate_poll'),  # Participate in a poll
    # path('manageFriends/', views.manage_friends, name='manage_friends'),  # Uncomment if manage friends feature is used
    path('feedback/', views.feedback, name='feedback'),  # Feedback view
    path('activate/<uidb64>/<token>/', activate_account, name='activate_account'),  # Account activation link

    path('', views.home, name="home"),  # This maps the root URL to the home view
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('logout/',views.user_logout,name="logout"),
    path('polls/create/', views.create_poll, name='create_poll'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('participate/', views.participate_view, name='participate_poll'),
    # path('manageFriends/', views.manage_friends, name='manage_friends'),
    path('feedback/', views.feedback, name= 'feedback'),
    path('activate/<uidb64>/<token>/', views.activate_account, name='activate_account'),


    # Poll-specific URLs
    path('<int:poll_id>/', views.detail, name='detail'),  # Poll details
    path('<int:poll_id>/vote/', views.vote, name='vote'),  # Vote in a poll
    path('<int:poll_id>/results/', views.results, name='results'),  # View poll results
    path('<int:poll_id>/report/', views.report_poll, name='report_poll'),  # Report a poll
    path('comments/<int:comment_id>/delete/', views.delete_comment, name='delete_comment'),
    
    # Uncomment if using CAPTCHA
    # path('captcha/', include('captcha.urls')),  # Add captcha functionality if needed

    # Add other paths as needed in the future
]
