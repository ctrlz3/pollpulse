# PollPulse
PollPulse is a web-based platform where users can create and participate in polls, view poll results, and report inappropriate polls. Built with Django, the project aims to provide an intuitive and user-friendly experience for poll management and interaction.
## Features

- **User Authentication**: Register, log in, and manage account details.
- **Poll Creation**: 
  - Create polls with multiple options.
  - Choose between single-choice or multiple-choice polls.
  - Add or delete options while creating the poll.
  - Set an end time for the poll.
- **Poll Participation**: View and participate in ongoing polls.
- **Report Polls**: Report polls that you find inappropriate.
- **Responsive Design**: Optimized for both desktop and mobile devices.
## Technology Used
- **Backend** : Django(Python)
- **Frontend** : HTML and CSS
- **Database** : SQlite
- **Version Control**: Git and Gitlab

## Setup
To get a local copy of PollPulse up and running, follow these steps:
### Prerequisites

- **Python 3.x**: Ensure Python is installed on your machine.
- **SQlite**: Install SQlite for database management.
- **Git**: Required for cloning the repository.

### Installation

1. **Clone the repository:**

   First, open your terminal and navigate to the directory where you want to clone the project. Then run the following command:

   ```bash
   git clone https://gitlab.com/your-username/pollpulse.git
   cd pollpulse
2. **Create a virtual environment:**

   It’s a good practice to create a virtual environment to manage dependencies. Run:

   ```bash
   python3 -m venv venv
   source venv/bin/activate

3. **Install the required Python packages:**

   Install all the dependencies listed in the requirements.txt file:

   ```bash
   pip install -r requirements.txt

4. **Install SQlite:**

   You can install SQLite using the package manager:

   ```bash
   sudo apt update
   sudo apt install sqlite3


5. **Apply the migrations:**

   Run the following command to create the necessary database tables:

   ```bash
   python manage.py migrate

6. **Create a superuser:**

   If you want to access the Django admin interface, create a superuser by running:

   ```bash
   python manage.py createsuperuser
7. **Run the development server:**

   Finally, start the Django development server:

   ```bash
   python manage.py runserver

8. **Access the application:**
   Open your web browser and go to http://127.0.0.1:8000 to view the PollPulse application.


## Usage

- **Home Page**: Overview of the platform with navigation links.
- **Create Poll**: 
  - Navigate to the dashboard and create a new poll.
  - Add or delete options, choose between single-choice or multiple-choice, and set an end time.
- **Participate in Polls**: View and participate in ongoing polls.
- **Report Polls**: Report any inappropriate polls by clicking on the report button.
- **Admin Interface**: Access the Django admin interface at `http://127.0.0.1:8000/admin` if you are a superuser.

## Project Structure

Here is an overview of the directory structure of the PollPulse project:

- **`manage.py`**: A command-line utility that lets you interact with this Django project.
- **`pollpulse/`**: The project’s main directory.
  - **`__init__.py`**: Indicates that this directory is a Python package.
  - **`settings.py`**: Contains project settings.
  - **`urls.py`**: Contains URL declarations for the project.
  - **`wsgi.py`**: An entry-point for WSGI-compatible web servers to serve your project.
- **`poll/`**: The Django app directory.
  - **`migrations/`**: Contains migration files for the app.
  - **`static/`**: Contains static files (CSS, images, JavaScript) for the app.
  - **`templates/`**: Contains HTML templates for the app.
  - **`views.py`**: Contains view functions for the app.
  - **`models.py`**: Contains models for the app.
  - **`admin.py`**: Contains admin interface configuration for the app.
  - **`urls.py`**: Contains URL declarations for the app.
- **`README.md`**: This file.

## Future Enhancements

- Implement deployment to a cloud platform.
- Add real-time notifications for poll updates.
- Integrate social media sharing for polls.
